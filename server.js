/*jslint node: true */

var mongoose = require('mongoose');
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var cookieSession = require('cookie-session');
var cors = require('cors');

var Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

console.log("Server init");
var mongoUrl = 'mongodb://localhost/comp3121ass1';
mongoose.connect(mongoUrl);

var app = express();
app.use(cookieParser());
app.use(cors());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.use(compression());
app.set('trust proxy', 1);

var NewsSchema = new Schema({
  newsId: {
    type: String,
    index: true
  },
  votes: [{
    fbUid: {
      type: String,
      index: {
        unique: true
      }
    },
    feeling: {
      type: String
    }
  }],
  voted: [],
  comments: [{}],
  voteStat: {}
});

var NewsModel = mongoose.model('news', NewsSchema);

var newsRouter = express.Router();
newsRouter.get('/:newsId/', function(req, res) {
  var query = {
    newsId: req.params.newsId
  };
  NewsModel.findOne(query, function(err, doc) {
    if (err) return res.send(500, {
      error: err
    });
    return res.send(doc);
  });
});

newsRouter.post('/:newsId/vote/', function(req, res) {

  var newVote = {
    fbUid: req.body.fbUid,
    feeling: req.body.feeling
  };
  /*check fbUid feeling*/
  if (!newVote.fbUid || !newVote.feeling)
    return res.sendStatus(500);
  /* try get record from db*/
  var query = {
    newsId: req.params.newsId
  };

  NewsModel.findOne(query, function(err, doc) {
    if (err != null) {
      console.log(err);
      return res.sendStatus(500);
    }
    if (doc !== null) {
      console.log(doc);

      if (doc.voted.indexOf(newVote.fbUid) != -1)
        res.sendStatus(500);
      else {
        doc.votes.push(newVote);
        doc.voted.push(newVote.fbUid);
        doc.save(function(err) {
          if (err) {
            console.log(err);
            return res.sendStatus(500);
          }
          res.sendStatus(200);
        });
      }
    }
    else {

      var news = new NewsModel({
        newsId: req.params.newsId,
        votes: [newVote],
        voted: [newVote.fbUid]
      });

      news.save(function(err) {
        if (err) {
          console.log(err);
          return res.sendStatus(500);
        }
        res.sendStatus(200);
      });
    }
  });
});

newsRouter.post('/:newsId/comment/', function(req, res) {
  var newComment = {
    fbUid: req.body.fbUid,
    comment: req.body.comment
  };
  /*check fbUid comment*/
  if (!newComment.fbUid || !newComment.comment)
    return res.sendStatus(500);
  /* try get record from db*/
  var query = {
    newsId: req.params.newsId
  };

  NewsModel.findOne(query, function(err, doc) {
    if (err != null) {
      return res.sendStatus(500);
    }

    if (doc === null) {
      doc = new NewsModel({
        newsId: req.params.newsId
      });
    }

    doc.comments.push(newComment);

    doc.save(function(err) {
      if (err) {
        console.log(err);
        return res.sendStatus(500);
      }
      res.sendStatus(200);
    });
  });
});

app.use('/api/news', newsRouter);

app.use(express.static('client/public'));

app.get('/', function(req, res) {
  res.sendStatus(200);
});

var port = process.env.PORT || 3000;
console.log("listen on " + port);
app.listen(port);
